import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppComponent } from "./app.component";
import { ListExampleComponent } from "./list-example/list-example.component";
import { ItemExampleComponent } from "./item-example/item-example.component";
import { DataExampleService } from "./data-example.service";
import { ItemResolverService } from "./item-resolver-service";
// import { NgShadowModule } from 'nativescript-ng-shadow';

import {
  routes
}
from "./app.routes";


import {
  NativeScriptRouterModule
}
from "nativescript-angular/router";


@NgModule({
  declarations: [AppComponent, ListExampleComponent, ItemExampleComponent],
  providers: [DataExampleService, ItemResolverService],
  bootstrap: [AppComponent],
  imports: [NativeScriptModule,
  NativeScriptRouterModule,
  NativeScriptRouterModule.forRoot(routes)],

  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
