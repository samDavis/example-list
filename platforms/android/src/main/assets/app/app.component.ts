import { Component } from "@angular/core";

@Component({
  selector: "my-app",
  template: `
    <GridLayout class="page">
      <page-router-outlet></page-router-outlet>
    </GridLayout>
  `
})
export class AppComponent {
  // Your TypeScript logic goes here
}
