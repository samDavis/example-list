"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_example_service_1 = require("../data-example.service");
var router_1 = require("@angular/router");
var ListExampleComponent = (function () {
    function ListExampleComponent(router, dataExampleService) {
        this.router = router;
        this.dataExampleService = dataExampleService;
    }
    ListExampleComponent.prototype.ngOnInit = function () {
        this.data = this.getItems();
    };
    ListExampleComponent.prototype.getItems = function () {
        return this.dataExampleService.getItems();
    };
    ListExampleComponent.prototype.navigateToItem = function (id) {
        this.router.navigate(["item", id]);
    };
    ListExampleComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-list-example',
            templateUrl: './list-example.component.html',
            styleUrls: ['./list-example.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router, data_example_service_1.DataExampleService])
    ], ListExampleComponent);
    return ListExampleComponent;
}());
exports.ListExampleComponent = ListExampleComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1leGFtcGxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpc3QtZXhhbXBsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsZ0VBQTZEO0FBQzdELDBDQUF5QztBQVF6QztJQUdFLDhCQUFvQixNQUFjLEVBQVUsa0JBQXNDO1FBQTlELFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO0lBQUksQ0FBQztJQUV2Rix1Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELHVDQUFRLEdBQVI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCw2Q0FBYyxHQUFkLFVBQWUsRUFBRTtRQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQWZVLG9CQUFvQjtRQU5oQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztTQUM3QyxDQUFDO3lDQUk0QixlQUFNLEVBQThCLHlDQUFrQjtPQUh2RSxvQkFBb0IsQ0FpQmhDO0lBQUQsMkJBQUM7Q0FBQSxBQWpCRCxJQWlCQztBQWpCWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRhRXhhbXBsZVNlcnZpY2UgfSBmcm9tIFwiLi4vZGF0YS1leGFtcGxlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICBzZWxlY3RvcjogJ2FwcC1saXN0LWV4YW1wbGUnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9saXN0LWV4YW1wbGUuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2xpc3QtZXhhbXBsZS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMaXN0RXhhbXBsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgZGF0YTtcclxuICBcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIGRhdGFFeGFtcGxlU2VydmljZTogRGF0YUV4YW1wbGVTZXJ2aWNlKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7IFxyXG4gICAgdGhpcy5kYXRhID0gdGhpcy5nZXRJdGVtcygpO1xyXG4gIH1cclxuXHJcbiAgZ2V0SXRlbXMoKXtcclxuICAgIHJldHVybiB0aGlzLmRhdGFFeGFtcGxlU2VydmljZS5nZXRJdGVtcygpO1xyXG4gIH1cclxuXHJcbiAgbmF2aWdhdGVUb0l0ZW0oaWQpe1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiaXRlbVwiLCBpZF0pO1xyXG4gIH1cclxuXHJcbn1cclxuIl19