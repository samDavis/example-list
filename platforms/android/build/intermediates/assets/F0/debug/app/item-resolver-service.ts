import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DataExampleService } from "./data-example.service";


@Injectable()
export class ItemResolverService implements Resolve<any> {
  constructor(private dataExampleService: DataExampleService) {}
  resolve(route: ActivatedRouteSnapshot) {
    return this.dataExampleService.getItem(route.params['id']);
  }
}