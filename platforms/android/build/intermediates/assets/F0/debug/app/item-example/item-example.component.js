"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_example_service_1 = require("../data-example.service");
var router_1 = require("@angular/router");
var ItemExampleComponent = (function () {
    function ItemExampleComponent(route, dataExampleService) {
        this.route = route;
        this.dataExampleService = dataExampleService;
    }
    ItemExampleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.forEach(function (data) {
            _this.item = data['item'];
        });
    };
    ItemExampleComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-item-example',
            templateUrl: './item-example.component.html',
            styleUrls: ['./item-example.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, data_example_service_1.DataExampleService])
    ], ItemExampleComponent);
    return ItemExampleComponent;
}());
exports.ItemExampleComponent = ItemExampleComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1leGFtcGxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIml0ZW0tZXhhbXBsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsZ0VBQTZEO0FBQzdELDBDQUFrRDtBQVFsRDtJQUVFLDhCQUFvQixLQUFxQixFQUFVLGtCQUFzQztRQUFyRSxVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUFVLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7SUFBSSxDQUFDO0lBRTlGLHVDQUFRLEdBQVI7UUFBQSxpQkFJQztRQUhDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDM0IsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBUlUsb0JBQW9CO1FBTmhDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsK0JBQStCO1lBQzVDLFNBQVMsRUFBRSxDQUFDLCtCQUErQixDQUFDO1NBQzdDLENBQUM7eUNBRzJCLHVCQUFjLEVBQThCLHlDQUFrQjtPQUY5RSxvQkFBb0IsQ0FXaEM7SUFBRCwyQkFBQztDQUFBLEFBWEQsSUFXQztBQVhZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRhRXhhbXBsZVNlcnZpY2UgfSBmcm9tIFwiLi4vZGF0YS1leGFtcGxlLnNlcnZpY2VcIjtcbmltcG9ydCB7ICBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbkBDb21wb25lbnQoe1xuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICBzZWxlY3RvcjogJ2FwcC1pdGVtLWV4YW1wbGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vaXRlbS1leGFtcGxlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaXRlbS1leGFtcGxlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSXRlbUV4YW1wbGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpdGVtO1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgcHJpdmF0ZSBkYXRhRXhhbXBsZVNlcnZpY2U6IERhdGFFeGFtcGxlU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7IFxuICAgIHRoaXMucm91dGUuZGF0YS5mb3JFYWNoKChkYXRhKSA9PiB7XG4gICAgICB0aGlzLml0ZW0gPSBkYXRhWydpdGVtJ107XG4gICAgfSk7XG4gIH1cblxuXG59XG4iXX0=