import { Component, OnInit } from '@angular/core';
import { DataExampleService } from "../data-example.service";
import { Router } from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-list-example',
  templateUrl: './list-example.component.html',
  styleUrls: ['./list-example.component.scss']
})
export class ListExampleComponent implements OnInit {
  data;
  
  constructor(private router: Router, private dataExampleService: DataExampleService) { }

  ngOnInit() { 
    this.data = this.getItems();
  }

  getItems(){
    return this.dataExampleService.getItems();
  }

  navigateToItem(id){
    this.router.navigate(["item", id]);
  }

}
