import { Component, OnInit } from '@angular/core';
import { DataExampleService } from "../data-example.service";
import {  ActivatedRoute } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-item-example',
  templateUrl: './item-example.component.html',
  styleUrls: ['./item-example.component.scss']
})
export class ItemExampleComponent implements OnInit {
  item;
  constructor(private route: ActivatedRoute, private dataExampleService: DataExampleService) { }

  ngOnInit() { 
    this.route.data.forEach((data) => {
      this.item = data['item'];
    });
  }


}
