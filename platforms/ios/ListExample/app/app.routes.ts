

import { ListExampleComponent } from "./list-example/list-example.component";
import { ItemExampleComponent } from "./item-example/item-example.component";
import { ItemResolverService } from "./item-resolver-service";

export const routes = [
  { path: "", component: ListExampleComponent },
  { path: "item/:id", component: ItemExampleComponent, resolve: {item: ItemResolverService} }
 // { path: "map", component: MapComponent }

  // { path: 'activity-manifest/:id', component: ActivityManifestComponent, resolve: {manifest: ActivityManifestResolverService}, canActivate: [AuthGuard]},
];

export const navigatableComponents = [
    ListExampleComponent, ItemExampleComponent
];