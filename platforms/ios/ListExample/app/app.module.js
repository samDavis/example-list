"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_component_1 = require("./app.component");
var list_example_component_1 = require("./list-example/list-example.component");
var item_example_component_1 = require("./item-example/item-example.component");
var data_example_service_1 = require("./data-example.service");
var item_resolver_service_1 = require("./item-resolver-service");
// import { NgShadowModule } from 'nativescript-ng-shadow';
var app_routes_1 = require("./app.routes");
var router_1 = require("nativescript-angular/router");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [app_component_1.AppComponent, list_example_component_1.ListExampleComponent, item_example_component_1.ItemExampleComponent],
            providers: [data_example_service_1.DataExampleService, item_resolver_service_1.ItemResolverService],
            bootstrap: [app_component_1.AppComponent],
            imports: [nativescript_module_1.NativeScriptModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forRoot(app_routes_1.routes)],
            schemas: [core_1.NO_ERRORS_SCHEMA],
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBRTlFLGlEQUErQztBQUMvQyxnRkFBNkU7QUFDN0UsZ0ZBQTZFO0FBQzdFLCtEQUE0RDtBQUM1RCxpRUFBOEQ7QUFDOUQsMkRBQTJEO0FBRTNELDJDQUdvQjtBQUdwQixzREFHbUM7QUFhbkM7SUFBQTtJQUF3QixDQUFDO0lBQVosU0FBUztRQVZyQixlQUFRLENBQUM7WUFDUixZQUFZLEVBQUUsQ0FBQyw0QkFBWSxFQUFFLDZDQUFvQixFQUFFLDZDQUFvQixDQUFDO1lBQ3hFLFNBQVMsRUFBRSxDQUFDLHlDQUFrQixFQUFFLDJDQUFtQixDQUFDO1lBQ3BELFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7WUFDekIsT0FBTyxFQUFFLENBQUMsd0NBQWtCO2dCQUM1QixpQ0FBd0I7Z0JBQ3hCLGlDQUF3QixDQUFDLE9BQU8sQ0FBQyxtQkFBTSxDQUFDLENBQUM7WUFFekMsT0FBTyxFQUFFLENBQUMsdUJBQWdCLENBQUM7U0FDNUIsQ0FBQztPQUNXLFNBQVMsQ0FBRztJQUFELGdCQUFDO0NBQUEsQUFBekIsSUFBeUI7QUFBWiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XG5cbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcbmltcG9ydCB7IExpc3RFeGFtcGxlQ29tcG9uZW50IH0gZnJvbSBcIi4vbGlzdC1leGFtcGxlL2xpc3QtZXhhbXBsZS5jb21wb25lbnRcIjtcbmltcG9ydCB7IEl0ZW1FeGFtcGxlQ29tcG9uZW50IH0gZnJvbSBcIi4vaXRlbS1leGFtcGxlL2l0ZW0tZXhhbXBsZS5jb21wb25lbnRcIjtcbmltcG9ydCB7IERhdGFFeGFtcGxlU2VydmljZSB9IGZyb20gXCIuL2RhdGEtZXhhbXBsZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBJdGVtUmVzb2x2ZXJTZXJ2aWNlIH0gZnJvbSBcIi4vaXRlbS1yZXNvbHZlci1zZXJ2aWNlXCI7XG4vLyBpbXBvcnQgeyBOZ1NoYWRvd01vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZy1zaGFkb3cnO1xuXG5pbXBvcnQge1xuICByb3V0ZXNcbn1cbmZyb20gXCIuL2FwcC5yb3V0ZXNcIjtcblxuXG5pbXBvcnQge1xuICBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVcbn1cbmZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtBcHBDb21wb25lbnQsIExpc3RFeGFtcGxlQ29tcG9uZW50LCBJdGVtRXhhbXBsZUNvbXBvbmVudF0sXG4gIHByb3ZpZGVyczogW0RhdGFFeGFtcGxlU2VydmljZSwgSXRlbVJlc29sdmVyU2VydmljZV0sXG4gIGJvb3RzdHJhcDogW0FwcENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRNb2R1bGUsXG4gIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcbiAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKV0sXG5cbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdLFxufSlcbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUge31cbiJdfQ==