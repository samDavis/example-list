"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_example_service_1 = require("./data-example.service");
var ItemResolverService = (function () {
    function ItemResolverService(dataExampleService) {
        this.dataExampleService = dataExampleService;
    }
    ItemResolverService.prototype.resolve = function (route) {
        return this.dataExampleService.getItem(route.params['id']);
    };
    ItemResolverService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [data_example_service_1.DataExampleService])
    ], ItemResolverService);
    return ItemResolverService;
}());
exports.ItemResolverService = ItemResolverService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1yZXNvbHZlci1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbS1yZXNvbHZlci1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRTNDLCtEQUE0RDtBQUk1RDtJQUNFLDZCQUFvQixrQkFBc0M7UUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtJQUFHLENBQUM7SUFDOUQscUNBQU8sR0FBUCxVQUFRLEtBQTZCO1FBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBSlUsbUJBQW1CO1FBRC9CLGlCQUFVLEVBQUU7eUNBRTZCLHlDQUFrQjtPQUQvQyxtQkFBbUIsQ0FLL0I7SUFBRCwwQkFBQztDQUFBLEFBTEQsSUFLQztBQUxZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmVzb2x2ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IERhdGFFeGFtcGxlU2VydmljZSB9IGZyb20gXCIuL2RhdGEtZXhhbXBsZS5zZXJ2aWNlXCI7XHJcblxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgSXRlbVJlc29sdmVyU2VydmljZSBpbXBsZW1lbnRzIFJlc29sdmU8YW55PiB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBkYXRhRXhhbXBsZVNlcnZpY2U6IERhdGFFeGFtcGxlU2VydmljZSkge31cclxuICByZXNvbHZlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KSB7XHJcbiAgICByZXR1cm4gdGhpcy5kYXRhRXhhbXBsZVNlcnZpY2UuZ2V0SXRlbShyb3V0ZS5wYXJhbXNbJ2lkJ10pO1xyXG4gIH1cclxufSJdfQ==